<h1 align="center">gw-lts</h1>

<p align="center">GW Low-latency Test Suite</p>

<p align="center">
<img src="https://git.ligo.org/rebecca.ewing/gw-lts/raw/main/static/gw-lts.png" alt="Centered Image" width="500" height="230">
</p>

<p align="center">
  <a href="https://git.ligo.org/rebecca.ewing/gw-lts/-/pipelines/latest">
    <img alt="ci" src="https://git.ligo.org/rebecca.ewing/gw-lts/badges/main/pipeline.svg" />
  </a>
  <a href="https://docs.ligo.org/rebecca.ewing/gw-lts/">
    <img alt="documentation" src="https://img.shields.io/badge/docs-mkdocs%20material-blue.svg?style=flat" />
  </a>
  <a href="https://pypi.org/project/gw-lts/">
    <img alt="pypi version" src="https://img.shields.io/pypi/v/gw-lts.svg" />
  </a>
</p>

---

## Features

The gw-lts software provides:

* real-time end-to-end consistency checks of science outputs from gravitational wave searches
* capability to simulate gravitational wave search outputs with a fake data set
* ability to display results with an example Grafana dashboard template

## Installation

```python3
pip install gw-lts
```
