#!/usr/bin/env python

import os
import yaml
from yaml.loader import SafeLoader

from gw.lts.utils import FARSTRINGS_DICT

from gstlal.dags import Option, Argument
from gstlal.dags import util as dagutil
from gstlal.dags.layers import Layer, Node


def get_datasource_options(source, ifos):
    return [
        Option("data-source", "devshm"),
        Option("shared-memory-dir",
               dagutil.format_ifo_args(ifos, source["shared-memory-dir"])),
        Option("shared-memory-block-size",
               source["shared-memory-block-size"]),
        Option("shared-memory-assumed-duration",
               source["shared-memory-assumed-duration"]),
        Option("channel-name",
               dagutil.format_ifo_args(ifos, source["channel-name"])),
        Option("state-channel-name",
               dagutil.format_ifo_args(ifos, source["state-channel-name"])),
        Option("dq-channel-name",
               dagutil.format_ifo_args(ifos, source["dq-channel-name"])),
        Option("state-vector-on-bits",
               dagutil.format_ifo_args(ifos, source["state-vector-on-bits"])),
        Option("state-vector-off-bits",
               dagutil.format_ifo_args(ifos, source["state-vector-off-bits"])),
        Option("dq-vector-on-bits",
               dagutil.format_ifo_args(ifos, source["dq-vector-on-bits"])),
        Option("dq-vector-off-bits",
               dagutil.format_ifo_args(ifos, source["dq-vector-off-bits"])),
    ]


def send_inj_stream_layer(dag, config, prefix):
    layer = Layer(
        os.path.join(prefix, "bin/send-inj-stream"),
        requirements={
            "request_cpus": 1,
            "request_memory": 2000,
            "request_disk": "1GB",
            **config["condor"]
        },
        retries=1000,
    )

    send_inj_stream_opts = config["jobs"]["send_inj_stream"]
    ifos = config["ifos"].split(",")
    analysis = config["analysis"]

    job_args = [
        Option("tag", config["tag"]),
        Option("kafka-server", config["kafka_server"]),
        Option("analysis", analysis),
        Option("ifo", ifos),
        Option("reference-psd", send_inj_stream_opts["reference-psd"]),
        Option("inj-file", config["injections"]),
    ]

    # add gw datasource options
    if analysis == "fake-data":
        if "fake-far-threshold" in send_inj_stream_opts.keys():
            job_args.append(Option("fake-far-threshold",
                            send_inj_stream_opts["fake-far-threshold"]))
        if "fake-inj-rate" in send_inj_stream_opts.keys():
            job_args.append(Option("fake-inj-rate",
                            send_inj_stream_opts["fake-inj-rate"]))
    else:
        source = config["source"]
        if not source["data-source"] == "devshm":
            raise ValueError(f"data source = {source['data-source']} not " +
                             "supported. You must use devshm.")

        gwdatasource_opts = get_datasource_options(source, ifos)
        job_args.extend(gwdatasource_opts)

    if "time-offset" in send_inj_stream_opts.keys():
        job_args.append(Option("time-offset",
                               send_inj_stream_opts["time-offset"]))
    if "f-max" in send_inj_stream_opts.keys():
        job_args.append(Option("f-max",
                               send_inj_stream_opts["f-max"]))
    if "verbose" in send_inj_stream_opts.keys():
        job_args.append(Option("verbose"))

    # add job to the dag
    layer += Node(arguments=job_args)
    dag.attach(layer)


def inspinjmsg_find_layer(dag, config, prefix):
    layer = Layer(
        os.path.join(prefix, "bin/inspinjmsg-find"),
        requirements={
            "request_cpus": 1,
            "request_memory": 2000,
            "request_disk": "1GB",
            **config["condor"]
        },
        retries=1000,
    )

    inspinjmsg_find_opts = config["jobs"]["inspinjmsg_find"]
    tag = config["tag"]
    source = config["analysis"]
    topics = [
        f"{source}.{tag}.testsuite.{topic}"
        for topic in inspinjmsg_find_opts["input_topic"]
    ]

    job_args = [
        Option("tag", config["tag"]),
        Option("kafka-server", config["kafka_server"]),
        Option("scald-config", config["metrics"][source]["config"]),
        Option("input-topic", topics),
    ]

    if "verbose" in inspinjmsg_find_opts.keys():
        job_args.append(Option("verbose"))

    layer += Node(arguments=job_args)
    dag.attach(layer)


def igwn_alert_listener_layer(dag, config, prefix):
    layer = Layer(
        os.path.join(prefix, "bin/igwn-alert-listener"),
        requirements={
            "request_cpus": 1,
            "request_memory": 2000,
            "request_disk": "1GB",
            **config["condor"]
        },
        retries=1000,
    )

    igwn_alert_opts = config["jobs"]["igwn_alert_listener"]
    job_args = [
        Option("tag", config["tag"]),
        Option("kafka-server", config["kafka_server"]),
        Option("gracedb-server", config["gracedb_server"]),
    ]

    for k, v in igwn_alert_opts.items():
        if k == "verbose":
            job_args.append(Option("verbose"))
            continue

        job_args.append(Option(k, v))

    layer += Node(arguments=job_args)
    dag.attach(layer)


def inj_missed_found_layer(dag, config, prefix):
    layer = Layer(
        os.path.join(prefix, "bin/inj-missed-found"),
        requirements={
            "request_cpus": 1,
            "request_memory": 2000,
            "request_disk": "1GB",
            **config["condor"]
        },
        retries=3,
    )

    tag = config["tag"]
    inj_missed_found_opts = config["jobs"]["inj_missed_found"]
    source = config["analysis"]
    topics = [
        f"{source}.{tag}.testsuite.{topic}"
        for topic in inj_missed_found_opts["input_topic"]
    ]

    job_args = [
        Option("tag", config["tag"]),
        Option("kafka-server", config["kafka_server"]),
        Option("scald-config", config["metrics"][source]["config"]),
        Option("input-topic", topics),
    ]

    if "verbose" in inj_missed_found_opts.keys():
        job_args.append(Option("verbose"))

    layer += Node(arguments=job_args)

    dag.attach(layer)


def vt_layer(dag, config, prefix):
    layer = Layer(
        os.path.join(prefix, "bin/vt"),
        requirements={
            "request_cpus": 1,
            "request_memory": 2000,
            "request_disk": "1GB",
            **config["condor"]
        },
        retries=3,
    )

    expected_vt_layer = Layer(
        os.path.join(prefix, "bin/vt"),
        name="expected_vt",
        requirements={
            "request_cpus": 1,
            "request_memory": 2000,
            "request_disk": "1GB",
            **config["condor"]
        },
        retries=3,
    )

    # for each analysis provided in the config we need to
    # add one calculate-expected job if that option is given
    # in the config. Then we need to add a job to compute VT
    # with each far threshold
    far_thresholds = FARSTRINGS_DICT.keys()
    tag = config["tag"]
    source = config["analysis"]
    vt_opts = config["jobs"]["vt"]
    topics = [
        f"{source}.{tag}.testsuite.{topic}" for topic in vt_opts["input_topic"]
    ]

    # add job arguments
    job_args = [
        Option("tag", config["tag"]),
        Option("analysis", source),
        Option("inj-file", config["injections"]),
        Option("kafka-server", config["kafka_server"]),
        Option("scald-config", config["metrics"][source]["config"]),
        Option("input-topic", topics),
    ]

    if "verbose" in vt_opts.keys():
        job_args.append(Option("verbose"))

    if "bootstrap-vt" in vt_opts.keys():
        job_args.append(Option("bootstrap-vt"))

    if "calculate-injected-vt" in vt_opts.keys():
        assert (
             "max-redshift" in vt_opts.keys()
        ), ("Must provide max-redshift in vt options if " +
            "calculate-injected-vt is given.")
        job_args.extend([
            Option("calculate-injected-vt"),
            Option("max-redshift", vt_opts["max-redshift"]),
        ])

    for far_thresh in far_thresholds:
        this_args = list(job_args)
        this_args.append(Option("far-threshold", far_thresh))
        layer += Node(arguments=this_args)

        if "calculate-expected" in vt_opts.keys():
            expected_vt_layer += Node(
                arguments=this_args + [Option("calculate-expected")]
            )

    dag.attach(layer)
    if "calculate-expected" in vt_opts.keys():
        dag.attach(expected_vt_layer)


def latency_layer(dag, config, prefix):
    layer = Layer(
        os.path.join(prefix, "bin/latency"),
        requirements={
            "request_cpus": 1,
            "request_memory": 2000,
            "request_disk": "1GB",
            **config["condor"]
        },
        retries=3,
    )

    latency_opts = config["jobs"]["latency"]
    tag = config["tag"]
    source = config["analysis"]
    topics = [
        f"{source}.{tag}.testsuite.{topic}"
        for topic in latency_opts["input_topic"]
    ]

    job_args = [
        Option("tag", config["tag"]),
        Option("kafka-server", config["kafka_server"]),
        Option("scald-config", config["metrics"][source]["config"]),
        Option("input-topic", topics),
    ]

    if "verbose" in latency_opts.keys():
        job_args.append(Option("verbose"))

    layer += Node(arguments=job_args)
    dag.attach(layer)


def likelihood_layer(dag, config, prefix):
    layer = Layer(
        os.path.join(prefix, "bin/likelihood"),
        requirements={
            "request_cpus": 1,
            "request_memory": 2000,
            "request_disk": "1GB",
            **config["condor"]
        },
        retries=3,
    )

    likelihood_opts = config["jobs"]["likelihood"]
    tag = config["tag"]
    source = config["analysis"]
    topics = [
        f"{source}.{tag}.testsuite.{topic}"
        for topic in likelihood_opts["input_topic"]
    ]

    job_args = [
        Option("tag", config["tag"]),
        Option("kafka-server", config["kafka_server"]),
        Option("scald-config", config["metrics"][source]["config"]),
        Option("input-topic", topics),
    ]

    if "verbose" in likelihood_opts.keys():
        job_args.append(Option("verbose"))

    layer += Node(arguments=job_args)
    dag.attach(layer)


def em_bright_layer(dag, config, prefix):
    layer = Layer(
        os.path.join(prefix, "bin/em-bright"),
        requirements={
            "request_cpus": 1,
            "request_memory": 2000,
            "request_disk": "1GB",
            **config["condor"]
        },
        retries=3,
    )

    embright_opts = config["jobs"]["em_bright"]
    tag = config["tag"]
    source = config["analysis"]
    topics = [
        f"{source}.{tag}.testsuite.{topic}"
        for topic in embright_opts["input_topic"]
    ]

    job_args = [
        Option("tag", config["tag"]),
        Option("kafka-server", config["kafka_server"]),
        Option("scald-config", config["metrics"][source]["config"]),
        Option("input-topic", topics),
        Option("gracedb-server", config["gracedb_server"]),
    ]

    if "verbose" in embright_opts.keys():
        job_args.append(Option("verbose"))

    layer += Node(arguments=job_args)
    dag.attach(layer)


def p_astro_layer(dag, config, prefix):
    layer = Layer(
        os.path.join(prefix, "bin/p-astro"),
        requirements={
            "request_cpus": 1,
            "request_memory": 2000,
            "request_disk": "1GB",
            **config["condor"]
        },
        retries=3,
    )

    pastro_opts = config["jobs"]["p_astro"]
    tag = config["tag"]
    source = config["analysis"]
    topics = [
        f"{source}.{tag}.testsuite.{topic}"
        for topic in pastro_opts["input_topic"]
    ]

    job_args = [
        Option("tag", config["tag"]),
        Option("kafka-server", config["kafka_server"]),
        Option("scald-config", config["metrics"][source]["config"]),
        Option("input-topic", topics),
    ]

    if "gdb-pastros" in pastro_opts.keys():
        job_args.extend([
            Option("gdb-pastros"),
            Option("gracedb-server", config["gracedb_server"]),
        ])

    if "pastro-file" in pastro_opts.keys():
        job_args.append(Option("pastro-file", pastro_opts["pastro-file"]))

    if "verbose" in pastro_opts.keys():
        job_args.append(Option("verbose"))

    layer += Node(arguments=job_args)
    dag.attach(layer)


def skymap_layer(dag, config, prefix):
    layer = Layer(
        os.path.join(prefix, "bin/skymap"),
        requirements={
            "request_cpus": 1,
            "request_memory": 2000,
            "request_disk": "1GB",
            **config["condor"]
        },
        retries=3,
    )

    skymap_opts = config["jobs"]["skymap"]
    tag = config["tag"]
    source = config["analysis"]
    topics = [
        f"{source}.{tag}.testsuite.{topic}"
        for topic in skymap_opts["input_topic"]
    ]

    job_args = [
        Option("tag", config["tag"]),
        Option("kafka-server", config["kafka_server"]),
        Option("scald-config", config["metrics"][source]["config"]),
        Option("input-topic", topics),
    ]

    if "gdb-skymaps" in skymap_opts.keys():
        job_args.extend([
            Option("gdb-skymaps"),
            Option("gracedb-server", config["gracedb_server"])
        ])

    if "output" in skymap_opts.keys():
        job_args.append(Option("output", skymap_opts["output"]))

    if "verbose" in skymap_opts.keys():
        job_args.append(Option("verbose"))

    layer += Node(arguments=job_args)
    dag.attach(layer)


def snr_consistency_layer(dag, config, prefix):
    layer = Layer(
        os.path.join(prefix, "bin/snr-consistency"),
        requirements={
            "request_cpus": 1,
            "request_memory": 2000,
            "request_disk": "1GB",
            **config["condor"]
        },
        retries=3,
    )

    snr_consistency_opts = config["jobs"]["snr_consistency"]
    ifos = config["ifos"].split(",")
    tag = config["tag"]
    source = config["analysis"]
    topics = [
        f"{source}.{tag}.testsuite.{topic}"
        for topic in snr_consistency_opts["input_topic"]
    ]

    job_args = [
        Option("tag", config["tag"]),
        Option("kafka-server", config["kafka_server"]),
        Option("scald-config", config["metrics"][source]["config"]),
        Option("input-topic", topics),
        Option("ifo", ifos),
    ]

    if "verbose" in snr_consistency_opts.keys():
        job_args.append(Option("verbose"))

    layer += Node(arguments=job_args)
    dag.attach(layer)


def inj_accuracy_layer(dag, config, prefix):
    layer = Layer(
        os.path.join(prefix, "bin/inj-accuracy"),
        requirements={
            "request_cpus": 1,
            "request_memory": 2000,
            "request_disk": "1GB",
            **config["condor"]
        },
        retries=3,
    )

    # add job options
    inj_accuracy_opts = config["jobs"]["inj_accuracy"]
    tag = config["tag"]
    source = config["analysis"]

    topics = [
        f"{source}.{tag}.testsuite.{topic}"
        for topic in inj_accuracy_opts["input_topic"]
    ]

    job_args = [
        Option("tag", config["tag"]),
        Option("kafka-server", config["kafka_server"]),
        Option("scald-config", config["metrics"][source]["config"]),
        Option("input-topic", topics),
        Option("input-params", inj_accuracy_opts["input_params"]),
    ]

    if "verbose" in inj_accuracy_opts.keys():
        job_args.append(Option("verbose"))

    layer += Node(arguments=job_args)

    dag.attach(layer)


def collect_metrics_layer(dag, config, prefix):
    layer = Layer(
        os.path.join(prefix, "bin/scald"),
        name="scald_metric_collector",
        requirements={
            "request_cpus": 1,
            "request_memory": 2000,
            "request_disk": "1GB",
            **config["condor"]
        },
        retries=3,
    )

    kafka_server = config["kafka_server"]
    tag = config["tag"]
    source = config["analysis"]
    web_config_path = config["metrics"][source]["config"]

    job_args = [
        Argument("command", "aggregate"),
        Option("uri", f"kafka://{tag}@{kafka_server}"),
        Option("config", web_config_path),
    ]

    # grab the web config
    with open(web_config_path, "r") as f:
        web_config = yaml.load(f, Loader=SafeLoader)

    metrics = [metric for metric in web_config["schemas"]]
    # these metrics are special, they dont get aggregated
    # by the metric collector
    metrics_to_remove = (
        "triggers", "missed_triggers", "analysis_start",
        "parameter_accuracy", "embright", "source_class",
        "sky_loc", "snr_accuracy", "latency",
    )
    for metric in metrics_to_remove:
        if metric in metrics:
            metrics.remove(metric)

    # add scald jobs to process all metrics in groups of 4
    for metric_group in scald_topics_grouper(metrics, 4):
        this_args = list(job_args)
        topics = []
        schemas = []
        for metric in metric_group:
            topics.append(f"{source}.{tag}.testsuite.{metric}")
            schemas.append(metric)

        this_args.extend([
            Option("data-type", "timeseries"),
            Option("topic", topics),
            Option("schema", schemas),
        ])
        layer += Node(arguments=this_args)
    dag.attach(layer)


def scald_topics_grouper(seq, size):
    return (seq[idx: idx + size] for idx in range(0, len(seq), size))
