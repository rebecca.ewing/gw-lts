#!/usr/bin/env python

from optparse import OptionParser

import yaml
from yaml.loader import SafeLoader

from gw.lts import dags as gwlts_dags

from gstlal.dags.inspiral import DAG


def parse_command_line():
    parser = OptionParser()
    parser.add_option(
        "-v", "--verbose", action="store_true", help="Be verbose."
    )
    parser.add_option("-c", "--config", help="Path to config.yml.")
    parser.add_option(
        "-n", "--name", default="test_suite", help="Name for the DAG file."
    )
    parser.add_option("-p", "--prefix", help="Prefix for executables")
    opts, args = parser.parse_args()

    return opts, args


class GWLTSDAG(object):
    def __init__(self, options):
        self.jobs = {
            "send_inj_stream": gwlts_dags.send_inj_stream_layer,
            "inspinjmsg_find": gwlts_dags.inspinjmsg_find_layer,
            "igwn_alert_listener": gwlts_dags.igwn_alert_listener_layer,
            "inj_missed_found": gwlts_dags.inj_missed_found_layer,
            "vt": gwlts_dags.vt_layer,
            "latency": gwlts_dags.latency_layer,
            "p_astro": gwlts_dags.p_astro_layer,
            "skymap": gwlts_dags.skymap_layer,
            "snr_consistency": gwlts_dags.snr_consistency_layer,
            "inj_accuracy": gwlts_dags.inj_accuracy_layer,
            "likelihood": gwlts_dags.likelihood_layer,
            "em_bright": gwlts_dags.em_bright_layer,
            "scald_metric_collector": gwlts_dags.collect_metrics_layer,
        }

        self.config_path = options.config
        self.prefix = options.prefix
        self.load_config(self.config_path)
        self.retries = "3"

        self.dag_name = options.name
        self.dag = DAG(config={})
        self.dag.create_log_dir()

        self.verbose = options.verbose

    def load_config(self, config_path):
        with open(config_path, "r") as f:
            self.config = yaml.load(f, Loader=SafeLoader)

        if ("environment" in self.config["condor"].keys() and
                isinstance(self.config["condor"]["environment"], dict)):
            env_opts = [f"{key}={val}" for
                        (key, val) in
                        self.config["condor"]["environment"].items()]
            self.config["condor"]["environment"] = f'"{" ".join(env_opts)}"'

    def create_dag(self):
        if self.verbose:
            print("Building DAG...")

        # add jobs
        for job in self.config["jobs"]:
            self.jobs[job](self.dag, self.config, self.prefix)

        # add a scald metric collector job for each datasource
        self.jobs["scald_metric_collector"](self.dag, self.config, self.prefix)

        # write out to file
        if self.verbose:
            print(f"Writing DAG out to file: {self.dag_name}.dag")
        self.dag.write_dag(f"{self.dag_name}.dag")
        self.dag.write_script(f"{self.dag_name}.sh")

        return self.dag


def main():
    # parse command line options
    opts, args = parse_command_line()

    if not opts.config:
        raise Exception("You need to specify a config file.")

    if not opts.prefix:
        raise Exception(
            "You need to specify the location of the executable files " +
            "with --prefix."
        )

    # build the dag
    dag_generator = GWLTSDAG(opts)
    dag_generator.create_dag()


if __name__ == "__main__":
    main()
