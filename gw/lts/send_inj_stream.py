#!/usr/bin/env python3

import copy
import io
import json
import logging
import numpy
import os
import sys
import threading

from collections import defaultdict, deque
from scipy.interpolate import interp1d
from time import sleep

from ligo.lw import ligolw
from ligo.lw import lsctables
from ligo.lw import utils as ligolw_utils
from ligo.lw.param import Param
from ligo.skymap.tool import bayestar_realize_coincs
from ligo.skymap.bayestar import filter as bayestar_filter

from gstlal import datasource, pipeio, psd
from gstlal.lloidhandler import SegmentsTracker as LLOIDSegmentsTracker
from gstlal.aggregator import now
from gstlal.stream import MessageType, Stream

import lal
from lal import LIGOTimeGPS, GreenwichMeanSiderealTime
import lal.series
import lalsimulation

from ligo.scald.io import kafka

from gw.lts import utils
from gw.lts.utils import pastro_utils


class LIGOLWContentHandler(ligolw.LIGOLWContentHandler):
    pass


lsctables.use_in(LIGOLWContentHandler)


def parse_command_line():
    parser = utils.add_general_opts()

    # generic "source" options
    datasource.append_options(parser)

    parser.add_option(
        "--time-offset",
        type=int,
        default=0,
        help=(
            "Time offset to shift injections by. Not used if --analysis "
            "is fake-data, required otherwise. If using mdc injections "
            "this should be a non-zero number that corresponds to the time "
            "h(t) is shifted by, otherwise, 0."
        ),
    )
    parser.add_option(
        "--ifo",
        action="append",
        help="IFOs to use. Can be given more than once."
    )
    parser.add_option(
        "--f-max",
        type=float,
        default=1600.0,
        help=(
            "Set the high frequency cut off for estimating"
            " the injection SNR."
        ),
    )
    parser.add_option(
        "--output-coinc",
        metavar="dir",
        default="output_files",
        help=(
            "the directory to output simulated coinc files to"
            " in the fake-data scheme."
        ),
    )
    parser.add_option(
        "--reference-psd",
        metavar="filename",
        help=(
            "Load spectrum from this LIGO light-weight XML file. "
            "The noise spectrum will be measured and tracked starting"
            "from this reference. (optional)."
        ),
    )
    parser.add_option(
        "--fake-inj-rate",
        type=float,
        default=20.0,
        help=(
            "Rate to send injection messages in the fake-data scheme."
            "Default is 20 seconds."
        ),
    )
    parser.add_option(
        "--fake-far-threshold",
        type=float,
        default=2.315e-5,
        help=(
            "Set the FAR threshold for sending simulated coincs in "
            "the fake-data scheme. Default is 2 per day."
        ),
    )

    options, filenames = parser.parse_args()

    return options, filenames


class SendInjStream(LLOIDSegmentsTracker):
    def __init__(self, instruments, tag, kafka_server, analysis, psd,
                 injection_file, stream=None, f_max=1600., offset=0,
                 coinc_output="output_files", fake_injection_rate=20.0,
                 fake_far_threshold=2.315e-5, verbose=False):
        logging.info("Setting up injection stream...")
        self.tag = tag
        self.kafka_server = kafka_server
        self.analysis = analysis
        self.ifos = instruments
        self.psd = psd
        self.f_max = f_max
        self.coinc_output = coinc_output
        self.fake_inj_rate = fake_injection_rate
        self.fake_far_threshold = fake_far_threshold
        self.offset = offset
        self.verbose = verbose

        # init kafka client
        self.producer = kafka.Client(f"kafka://{self.tag}@{self.kafka_server}")

        # load and sort sim inspiral table
        xmldoc = ligolw_utils.load_filename(
            injection_file, contenthandler=LIGOLWContentHandler
        )
        self.simtable = lsctables.SimInspiralTable.get_table(xmldoc)
        self.simtable.sort(
            key=lambda row: row.geocent_end_time
            + 10.0**-9.0 * row.geocent_end_time_ns
        )

        # if streaming data, set up the segments tracker
        # and find the nearest injection to start from
        if stream:
            self.lock = threading.Lock()
            LLOIDSegmentsTracker.__init__(
                self,
                stream,
                instruments,
                verbose=verbose
            )

            # init dict to store IFO segments
            self.state_history = {
                ifo: deque(maxlen=1000) for ifo in instruments
            }
            self.time_of_last_state = None

            # skip any old injections first
            t = now()
            for row in list(self.simtable):
                injection_time = row.geocent_end_time + self.offset
                # skip injections which have already passed
                if t - injection_time > 0:
                    logging.debug(
                        f"Skipping old injection (t = {injection_time})"
                    )
                    self.simtable.pop(0)
                else:
                    break
            assert len(self.simtable), "No remaining injections left in " \
                                       "simtable after skipping ones in " \
                                       "the past. Check your --time-offset."

        # otherwise, do additional setup required for
        # simulating coinc events in the fake-data scheme
        else:
            self.fake_injection_stream_setup()

    def on_buffer(self, buf, instrument):
        # collect IFO state history every second
        t = now()
        if not self.time_of_last_state:
            self.time_of_last_state = t

        if t - self.time_of_last_state >= 1:
            gate_segments = self.collect_gate_segments(
                self.time_of_last_state, t + 1)

            # update time of last state
            self.time_of_last_state = t

            for ifo, segs in gate_segments.items():
                self.state_history[ifo].extend(segs)

                # write data to kafka
                times, states = zip(*self.state_history[ifo])
                outdata = {
                    'time': times,
                    'data': states,
                }

                logging.debug(f"Writing {ifo} states to kafka ...")
                topic = f"{self.analysis}.{self.tag}.testsuite.{ifo}_state"
                self.producer.write(topic, outdata)

        # check if its time for next injection
        with self.lock:
            thisrow = self.simtable[0]
            injection_time = thisrow.geocent_end_time + self.offset
            # if injection is in the future, wait a little longer
            if t - injection_time < 0:
                return
            # if injection is too far in the past, skip it
            # FIXME: consider making this 60 second skip time user configurable
            elif t - injection_time > 60:
                logging.warning(f"Skipping injection ({injection_time}) " +
                                "from more than 60 seconds ago.")
                self.simtable.pop(0)
                return

            # first check segments for this injection time to get IFO states
            ifo_states = defaultdict(lambda: None)
            for ifo in self.ifos:
                # find nearest times before and after the injection time
                try:
                    before_time, before_diff = min(
                        [(t, abs(t - injection_time))
                            for t, state in self.state_history[ifo]
                            if t < injection_time], key=lambda x: x[1])
                    after_time, after_diff = min(
                        [(t, t - injection_time)
                            for t, state in self.state_history[ifo]
                            if t > injection_time], key=lambda x: x[1])
                except ValueError:
                    # if we dont have any states from before the injection
                    # time, we cant determine the state for this ifo.
                    # if we dont have any states after the injection time,
                    # we need to wait for the next buffer. Either way,
                    # we can continue to the next IFO for now.
                    continue

                if before_diff >= 1.:
                    logging.warning(f"Closest {ifo} state before the " +
                                    f"injection at {injection_time} is " +
                                    f"{before_diff} seconds away.")
                if after_diff >= 1.:
                    logging.warning(f"Closest {ifo} state after the " +
                                    f"injection at {injection_time} is " +
                                    f"{after_diff} seconds away.")

                # find the IFO states close to the injection time
                near_states = set(
                    [state for t, state in self.state_history[ifo]
                        if before_time <= t and after_time >= t])

                if len(near_states) > 1:
                    # if there was a state change near the injection time,
                    # whether the IFO switched from on-->off or off-->on,
                    # lets just count it as off as a conservative way to
                    # deal with edge effects
                    # FIXME we could probably be more careful about this
                    ifo_on = False
                else:
                    # otherwise, determine the IFO state
                    ifo_on = all(near_states)

                ifo_states[ifo] = ifo_on
                logging.debug(f"{ifo} {'ON' if ifo_on else 'OFF'} at " +
                              f"{injection_time}")
            # if we didnt find the state for all IFOs, return
            # and we will try again on the next buffer
            if not len(ifo_states.keys()) == len(self.ifos):
                return

            # once we have the IFO states, proceed to process this injection
            logging.debug("Processing injection at coa time: " +
                          f"{injection_time}...")

            onIFOs = [ifo for ifo, state in ifo_states.items() if state]
            logging.debug(f"on IFOs: {onIFOs}")

            # generate and produce output
            output_row, outxml = self.produce_output_simrow(thisrow)
            self.produce_injection_message(outxml, onIFOs)

            # finally pop this injection from the table once we are done
            # processing it
            self.simtable.pop(0)

    def fake_injection_stream_start(self):
        logging.info("Starting injection stream...")
        # step through sim table row by row, update sim col values as
        # necessary, send message to inj_stream, then generate a coinc
        # file and send a message to events topic
        while self.simtable:
            # remove and return the oldest/first inj in the table
            thisrow = self.simtable.pop(0)

            # sleep for inj cadence
            sleep(self.fake_inj_rate)

            timenow = now()
            time_offset = timenow - thisrow.geocent_end_time
            injection_time = thisrow.geocent_end_time + time_offset

            logging.debug("Processing injection at coalescence time: " +
                          f"{injection_time}.")

            # assume all IFOs are always on
            onIFOs = ["H1", "L1", "V1"]

            output_row, outxml = self.produce_output_simrow(thisrow)
            trigger = copy.copy(output_row)

            self.produce_injection_message(outxml, onIFOs)

            # Simulate a coinc FAR given the SNR. This is
            # set so that a network SNR 7 event (H1 SNR = L1 SNR = 4.)
            # is recovered with FAR < 2/day.
            snrs = [snr for snr in
                    (trigger.alpha4, trigger.alpha5, trigger.alpha6)
                    if snr > 4.0]
            net_snr = numpy.sqrt(numpy.linalg.norm(snrs))
            coincfar = 6 * 10**-4.0 * numpy.exp(-(net_snr)**2.0 / 2.0)

            # produce a fake coinc if the far passes the threshold
            if coincfar < self.fake_far_threshold:
                logging.debug("Sending a coinc trigger...")
                self.produce_coinc_message(trigger, coincfar)

        logging.info("Sent all injections. Exiting ...")

    def produce_output_simrow(self, thisrow):
        # proceed to generate the correct sim row and send
        # an output message to kafka
        outxml = ligolw.Document()
        outxml.appendChild(ligolw.LIGO_LW())
        output_simtable = lsctables.New(lsctables.SimInspiralTable)

        output_row = copy.copy(thisrow)

        # if time offset is nonzero, shift the times, longitude
        # and re-calculate the inj snrs
        if self.offset:
            output_row = self.shift_times(output_row, self.offset)
        inj_snrs = self.calc_inj_snrs(output_row)

        # add inj snrs to appropriate cols in output_row
        for (col_name, ifo) in zip(("alpha4", "alpha5", "alpha6"),
                                   ("H1", "L1", "V1")):
            if ifo not in self.ifos:
                continue
            setattr(output_row, col_name, inj_snrs[ifo])

        logging.debug(
            f"SNRs: H1: {output_row.alpha4} | L1: {output_row.alpha5}"
            f" | V1: {output_row.alpha6}"
        )

        # construct output message packet
        output_simtable.append(output_row)
        outxml.childNodes[-1].appendChild(output_simtable)

        return output_row, outxml

    def produce_injection_message(self, outxml, onIFOs):
        sim_msg = io.BytesIO()
        ligolw_utils.write_fileobj(outxml, sim_msg)

        output = {
           "sim": sim_msg.getvalue().decode(),
           "onIFOs": (",").join(onIFOs)
        }

        # send message to kafka
        topic = f"{self.analysis}.{self.tag}.testsuite.inj_stream"
        self.producer.write(topic, output)
        logging.info(f"Sent msg to: {topic}")

        outxml.unlink()

        return

    def shift_times(self, row, time_offset):
        """
        fix RA and GPS times according to the time offset

        Parameters
        ----------
        row (ligolw table row)
            SimInspiral table row object corresponding to
            a single injection.
        time_offset (int)
            Offset to shift injection times by.

        Returns
        ----------
        row (ligolw table row)
            SimInspiral table row with shifted times and
            corrected right ascension
        """
        end_time = row.geocent_end_time + row.geocent_end_time_ns * 10.0**-9.0
        gmst0 = GreenwichMeanSiderealTime(LIGOTimeGPS(end_time))
        gmst = GreenwichMeanSiderealTime(LIGOTimeGPS(end_time + time_offset))
        dgmst = gmst - gmst0
        row.longitude = row.longitude + dgmst

        row.geocent_end_time = int(row.geocent_end_time + time_offset)
        row.h_end_time = row.h_end_time + time_offset
        row.l_end_time = row.l_end_time + time_offset
        row.v_end_time = row.v_end_time + time_offset

        return row

    def calc_inj_snrs(self, inj):
        """
        Estimate injected SNRs given injection time,
        waveform, intrinsic and extrinsic parameters.

        Parameters
        ----------
        inj (ligolw table row)
            SimInspiral table row object corresponding to
            a single injection.

        Returns
        ----------
        snr (dict)
            Dictionary, keyed by ifo, of estimated injected
            SNRs.
        """
        snr = dict.fromkeys(self.ifos, 0.0)

        injtime = inj.geocent_end_time
        f_min = inj.f_lower
        approximant = lalsimulation.GetApproximantFromString(str(inj.waveform))
        sample_rate = 16384.0
        f_max = self.f_max

        h_plus, h_cross = lalsimulation.SimInspiralTD(
            m1=inj.mass1 * lal.MSUN_SI,
            m2=inj.mass2 * lal.MSUN_SI,
            S1x=inj.spin1x,
            S1y=inj.spin1y,
            S1z=inj.spin1z,
            S2x=inj.spin2x,
            S2y=inj.spin2y,
            S2z=inj.spin2z,
            distance=inj.distance * 1e6 * lal.PC_SI,
            inclination=inj.inclination,
            phiRef=inj.coa_phase,
            longAscNodes=0.0,
            eccentricity=0.0,
            meanPerAno=0.0,
            deltaT=1.0 / sample_rate,
            f_min=f_min,
            f_ref=0.0,
            LALparams=None,
            approximant=approximant,
        )

        h_plus.epoch += injtime
        h_cross.epoch += injtime

        # Compute strain in each detector. If one detector wasn't on,
        # snr will be set to zero.
        for instrument in snr:
            if instrument not in self.psd.keys():
                continue
            h = lalsimulation.SimDetectorStrainREAL8TimeSeries(
                h_plus,
                h_cross,
                inj.longitude,
                inj.latitude,
                inj.polarization,
                lalsimulation.DetectorPrefixToLALDetector(instrument),
            )
            snr[instrument] = lalsimulation.MeasureSNR(
                h, self.psd[instrument], f_min, f_max
            )

        return snr

    def collect_gate_segments(self, t_start, t_end):
        gatesegments = defaultdict(lambda: [])
        # NOTE: for our purposes we only care about statevectorsegments
        # and not the other gates
        for instrument, seg_history in (
            self.gate_history["statevectorsegments"].items()
        ):
            if not seg_history:
                continue
            # get on/off points, add point at +inf
            gate_interp_times, gate_interp_onoff = zip(*seg_history)
            gate_interp_times = list(gate_interp_times)
            gate_interp_times.append(2000000000)
            gate_interp_onoff = list(gate_interp_onoff)
            gate_interp_onoff.append(gate_interp_onoff[-1])

            # regularly sample from on/off points
            gate_times = numpy.arange(int(t_start), int(t_end), 0.25)
            gate_onoff = interp1d(
                gate_interp_times, gate_interp_onoff, kind='zero')(gate_times)
            gatesegments[instrument].extend(
                [(t, state) for t, state in zip(gate_times, gate_onoff)
                    if t >= t_start])

        return gatesegments

    def fake_injection_stream_setup(self):
        # set up reference psd
        psdxmldoc = ligolw_utils.load_filename(
            self.psd, contenthandler=lal.series.PSDContentHandler
        )
        self.psd = lal.series.read_psd_xmldoc(psdxmldoc, root_name=None)

        self.psds_dict = {
            key: bayestar_filter.InterpolatedPSD(
                bayestar_filter.abscissa(psd), psd.data.data
            )
            for key, psd in self.psd.items()
            if psd is not None
        }

        # Set-up some data products needed for simulating
        # coinc events in the "fake-data" scheme. Produce
        # an interpolated PSD object used to simulate SNR
        # time-series and compute prior distributions for
        # simulated p(astro).
        self.detectors = [
            lalsimulation.DetectorPrefixToLALDetector(ifo)
            for ifo in self.ifos
        ]
        self.responses = [det.response for det in self.detectors]
        self.locations = [det.location for det in self.detectors]

        # the interpolated object is used for SNR time series simulation
        self.psds_interp = [self.psds_dict[ifo] for ifo in self.ifos]

        # compute distributions required for pastro calculation
        self.p_x_c = pastro_utils.p_x_c(
            bns=(1.22, 0.06), nsbh=(6.27, 1.84), bbh=(42.98, 8.11)
        )
        self.p_c = pastro_utils.p_c(
            self.p_x_c,
            N_events={
                "Terrestrial": 0,
                "BNS": 60800,
                "NSBH": 48400,
                "BBH": 60800},
        )

    def produce_coinc_message(self, trigger, coincfar):
        """
        Send output messages of simulated coinc events to output
        `inj_events` topic.

        Parameters
        ----------
        trigger (ligowl table row)
            Row corresponding to a single injection in the SimInspiral
            table.
        coincfar (float)
            FAR value for simulated recovered event associated with
            this injection.
        """
        # build coinc xml doc, calculate p_astro, and produce message
        newxmldoc = self.produce_output_coinc(trigger, coincfar)
        if not newxmldoc:
            return

        coinctable = lsctables.CoincInspiralTable.get_table(newxmldoc)
        coincrow = coinctable[0]

        time = coincrow.end_time
        coincsnr = coincrow.snr
        mchirp = coincrow.mchirp
        p_astro = pastro_utils.p_astro(mchirp, coincsnr, self.p_x_c, self.p_c)

        # write coinc file to disk
        filename = f"fake_coinc-{int(time)}.xml"
        ligolw_utils.write_filename(
            newxmldoc,
            os.path.join(self.coinc_output, filename),
            verbose=self.verbose
        )

        coinc_msg = io.BytesIO()
        ligolw_utils.write_fileobj(newxmldoc, coinc_msg)

        # create json packet
        output = {
            "time": time,
            "time_ns": coincrow.end_time_ns,
            "snr": coincsnr,
            "far": coincrow.combined_far,
            "p_astro": json.dumps(p_astro),
            "coinc": coinc_msg.getvalue().decode(),
        }

        # send coinc message to events topic
        logging.info(f"network SNR: {output['snr']} | FAR: {output['far']}")

        topic = f"fake-data.{self.tag}.testsuite.inj_events"
        self.producer.write(topic, output)
        logging.info(f"Sent msg to: {topic}")

        newxmldoc.unlink()

        return

    def produce_output_coinc(self, row, coincfar):
        """
        Construct a full ligolw file object to represent a simulated
        recovered event.

        Parameters
        ----------
        row (ligolw table row)
            A single SimInspiral row corresponding to the injection.
        coincfar (float)
            FAR value for simulated recovered event associated with
            this injection.

        Returns
        ----------
        newxmldoc (ligolw file object)
            ligo-lw coinc file object with all required tables.
        """
        # instantiate relevant lsctables objects
        newxmldoc = ligolw.Document()
        ligolw_elem = newxmldoc.appendChild(ligolw.LIGO_LW())
        new_process_table = ligolw_elem.appendChild(
            lsctables.New(lsctables.ProcessTable,
                          columns=utils.all_process_rows)
        )
        new_sngl_inspiral_table = ligolw_elem.appendChild(
            lsctables.New(lsctables.SnglInspiralTable,
                          columns=utils.all_sngl_rows)
        )
        new_coinc_inspiral_table = ligolw_elem.appendChild(
            lsctables.New(lsctables.CoincInspiralTable,
                          columns=utils.all_coinc_rows)
        )
        new_coinc_event_table = ligolw_elem.appendChild(
            lsctables.New(lsctables.CoincTable)
        )
        new_coinc_map_table = ligolw_elem.appendChild(
            lsctables.New(lsctables.CoincMapTable)
        )

        # simulate SNR time series using interpolated psd object
        # measurement_error is set as gaussian but one can switch
        # to no noise by measurement_error="zero-noise"
        bayestar_sim_list = bayestar_realize_coincs.simulate(
            seed=None,
            sim_inspiral=row,
            psds=self.psds_interp,
            responses=self.responses,
            locations=self.locations,
            measurement_error="gaussian-noise",
            f_low=20,
            f_high=2048,
        )

        # get mass parameters
        mass1 = max(numpy.random.normal(loc=row.mass1, scale=1.0), 1.1)
        mass2 = max(numpy.random.normal(loc=row.mass2, scale=1.0), mass1)
        mchirp, eta = self.mc_eta_from_m1_m2(mass1, mass2)

        snrs = defaultdict(lambda: 0)
        coincsnr = None

        # populate process table
        process_row_dict = {k: 0 for k in utils.all_process_rows}
        process_row_dict.update(
            {"process_id": 0, "program": "gstlal_inspiral", "comment": ""}
        )
        new_process_table.extend([
            lsctables.ProcessTable.RowType(**process_row_dict)
        ])

        # populate sngl table, coinc map table, and SNR timeseriess
        for event_id, (
            ifo, (horizon, abs_snr, arg_snr, toa, snr_series)
        ) in enumerate(zip(self.ifos, bayestar_sim_list)):
            sngl_row_dict = {k: 0 for k in utils.all_sngl_rows}

            sngl_row_dict.update(
                {
                    "process_id": 0,
                    "event_id": event_id,
                    "end": toa,
                    "mchirp": mchirp,
                    "mass1": mass1,
                    "mass2": mass2,
                    "eta": eta,
                    "ifo": ifo,
                    "snr": abs_snr,
                    "coa_phase": arg_snr,
                }
            )

            # add to the sngl inspiral table
            new_sngl_inspiral_table.extend(
                [lsctables.SnglInspiralTable.RowType(**sngl_row_dict)]
            )
            snrs[ifo] = abs_snr

            coinc_map_row_dict = {
                "coinc_event_id": 0,
                "event_id": event_id,
                "table_name": "sngl_inspiral",
            }

            # add to the coinc map table
            new_coinc_map_table.extend(
                [lsctables.CoincMapTable.RowType(**coinc_map_row_dict)]
            )

            # add SNR time series as array objects
            elem = lal.series.build_COMPLEX8TimeSeries(snr_series)
            elem.appendChild(Param.from_pyvalue("event_id", event_id))
            ligolw_elem.appendChild(elem)

        # calculate coinc SNR, only proceed if above 4
        coincsnr = numpy.linalg.norm([snr for snr in snrs.values() if snr > 4])
        if not coincsnr:
            logging.debug(f"Coinc SNR {coincsnr} too low to send a message.")
            return None

        # populate coinc inspiral table
        coinc_row_dict = {col: 0 for col in utils.all_coinc_rows}
        coincendtime = row.geocent_end_time
        coincendtimens = row.geocent_end_time_ns
        coinc_row_dict.update(
            {
                "coinc_event_id": 0,
                "snr": coincsnr,
                "mass": row.mass1 + row.mass2,
                "mchirp": row.mchirp,
                "end_time": coincendtime,
                "end_time_ns": coincendtimens,
                "combined_far": coincfar,
            }
        )
        new_coinc_inspiral_table.extend(
            [lsctables.CoincInspiralTable.RowType(**coinc_row_dict)]
        )

        # populate coinc event table
        coinc_event_row_dict = {col: 0 for col in utils.all_coinc_event_rows}
        coinc_event_row_dict.update(
            {
                "coinc_def_id": 0,
                "process_id": 0,
                "time_slide_id": 0,
                "instruments": "H1,L1,V1",
                "numevents": len(new_sngl_inspiral_table),
            }
        )
        new_coinc_event_table.extend(
            [lsctables.CoincTable.RowType(**coinc_event_row_dict)]
        )

        # add psd frequeny series
        lal.series.make_psd_xmldoc(self.psd, ligolw_elem)

        return newxmldoc

    def on_spectrum_message(self, message):
        psd = pipeio.parse_spectrum_message(message)
        self.psd[psd.name] = psd
        logging.debug(f"Updated {psd.name} psd.")
        return True

    def H1_on_buffer(self, buf):
        return self.on_buffer(buf, "H1")

    def L1_on_buffer(self, buf):
        return self.on_buffer(buf, "L1")

    def V1_on_buffer(self, buf):
        return self.on_buffer(buf, "V1")

    def K1_on_buffer(self, buf):
        return self.on_buffer(buf, "K1")

    @staticmethod
    def mc_eta_from_m1_m2(m1, m2):
        """
        Compute chirp mass and mass ratio from component masses.
        """
        mc = (m1 * m2) ** (3.0 / 5.0) / (m1 + m2) ** (1.0 / 5.0)
        eta = (m1 * m2) / (m1 + m2) ** 2.0

        return mc, eta


def main():
    # parse command line
    opts, args = parse_command_line()

    # in the fake-data scheme, create a directory
    # to output simulated coincs to. This is not
    # needed otherwise
    if opts.analysis == "fake-data":
        try:
            os.mkdir(opts.output_coinc)
        except OSError:
            pass

    # set up logger
    utils.set_up_logger(opts.verbose)

    # if streaming data, build pipeline
    if not opts.analysis == "fake-data":
        # parse the generic "source" options, check for inconsistencies
        # is done inside the class init method
        gw_data_source_info = datasource.DataSourceInfo.from_optparse(opts)

        instruments = list(gw_data_source_info.channel_dict.keys())

        if opts.reference_psd:
            # FIXME check for all required IFOs
            reference_psd = {
                ifo: psd.read_psd(opts.reference_psd)[ifo]
                for ifo in instruments
            }
        else:
            # FIXME is this the right format for initialing no ref PSD?
            reference_psd = {ifo: None for ifo in instruments}

        logging.info("building pipeline ...")
        stream = Stream.from_datasource(
            gw_data_source_info,
            instruments,
            state_vector=bool(gw_data_source_info.state_channel_name),
            dq_vector=bool(gw_data_source_info.dq_channel_name),
            idq_series=bool(gw_data_source_info.idq_channel_name),
            verbose=opts.verbose
        )

        # set up injection streamer
        injection_stream = SendInjStream(
            instruments, opts.tag, opts.kafka_server,
            opts.analysis, reference_psd, opts.inj_file, stream=stream,
            f_max=opts.f_max, offset=opts.time_offset, verbose=opts.verbose
            )

        stream.add_callback(
            MessageType.ELEMENT,
            "spectrum",
            injection_stream.on_spectrum_message
        )

        # set up the stream for each ifo
        for instrument, this_stream in stream.items():
            this_stream.resample(quality=9) \
                .queue(max_size_buffers=8) \
                .condition(
                    4096,
                    instrument,
                    psd=reference_psd[instrument],
                    psd_fft_length=16,
                    track_psd=True,
                    statevector=stream.source.state_vector[instrument],
                    dqvector=stream.source.dq_vector[instrument]
                    ) \
                .queue() \
                .reblock() \
                .bufsink(eval(f"injection_stream.{instrument}_on_buffer"))

        # start pipeline
        logging.info("running pipeline ...")
        stream.start()

    # initialize and set up for the fake-data scheme
    else:
        injection_stream = SendInjStream(
            opts.ifo, opts.tag, opts.kafka_server,
            opts.analysis, opts.reference_psd, opts.inj_file,
            f_max=opts.f_max, coinc_output=opts.output_coinc,
            fake_injection_rate=opts.fake_inj_rate,
            fake_far_threshold=opts.fake_far_threshold,
            verbose=opts.verbose
            )

        # start the stream
        injection_stream.fake_injection_stream_start()

    # done.
    logging.info("shutting down...")
    sys.exit(1)


if __name__ == '__main__':
    main()
