#!/usr/bin/env python3

import json
import logging

from cronut import App

from gw.lts import utils
from gw.lts.utils import influx_helper as influx_utils


def parse_command_line():
    parser = utils.add_general_opts()
    opts, args = parser.parse_args()

    return opts, args


def _new_trigger():
    dict = {}
    columns = (
        "combined_far",
        "latency",
    )
    for col in columns:
        dict[col] = None

    # we will initialize the combined far value to
    # an arbitrary high value which will get replaced
    # with the actual far from events
    dict["combined_far"] = 1.0

    return dict


def main():
    opts, args = parse_command_line()

    # set up logging
    utils.set_up_logger(opts.verbose)

    tag = opts.tag

    # initialize influx helper to write out trigger data
    influx_helper = influx_utils.InfluxHelper(
        config_path=opts.scald_config,
        routes={
            "latency": {"aggregate": "max"},
        },
    )

    # create a job service using cronut
    app = App("latency", broker=f"kafka://{tag}_latency@{opts.kafka_server}")

    # subscribes to a topic
    @app.process(opts.input_topic)
    def process(message):
        mdatasource, mtag, mtopic = utils.parse_msg_topic(message)
        logging.debug(f"Read message from {mdatasource} {mtopic}.")

        # parse event info
        event = json.loads(message.value())

        time = event["time"] + event["time_ns"] * 10**-9.0
        latency = event["latency"]
        snr_optimized = event["snr_optimized"]

        trigger_dict = _new_trigger()
        trigger_dict["latency"] = latency
        trigger_dict["combined_far"] = event["far"]

        # store trigger data to influx
        influx_helper.store_triggers(
            time,
            trigger_dict,
            route="latency",
            tags=(snr_optimized)
        )

    # start up
    logging.info("Starting up...")
    app.start()


if __name__ == "__main__":
    main()
